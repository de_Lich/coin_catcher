﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] float runSpeed = 100;
    [SerializeField] float turnSpeed = 100;

    void Update()
    {
        Vector3 moveforward = transform.position += transform.forward * runSpeed * Time.deltaTime;
        //Rotate();
        OffSet(moveforward);
    }

    private void OffSet(Vector3 moveforward)
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.position -= transform.right * runSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += transform.right * runSpeed * Time.deltaTime;
        }
    }

    private void Rotate()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(new Vector3(0, -1) * turnSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(new Vector3(0, 1) * turnSpeed * Time.deltaTime);
        }
    }
}
