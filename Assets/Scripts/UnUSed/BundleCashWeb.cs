﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BundleCashWeb : MonoBehaviour
{
    private string url = "https://s3.eu-north-1.amazonaws.com/com.adelich.addressebles/Android/prefab.1";
    Action<AssetBundle> response;
    void Start()
    {
        StartCoroutine(LoadAssetBundleFromServerWithCache(url, response));
    }
    IEnumerator LoadAssetBundleFromServerWithCache(string url, Action<AssetBundle> response)
    {
        //waiting for cash-system
        while (!Caching.ready)
        {
            yield return null;
        }

        //get manifest from server
        var request = UnityWebRequest.Get(url + ".json");
        yield return request.SendWebRequest();

        if(!request.isHttpError && !request.isNetworkError)
        {
            Hash128 hash = default;

            //getting hash
            var hashRow = request.downloadHandler.text.ToString().Split("\n".ToCharArray())[5];
            hash = Hash128.Parse(hashRow.Split(':')[1].Trim());

            if (hash.isValid == true)
            {
                request.Dispose();
                request = UnityWebRequestAssetBundle.GetAssetBundle(url, hash, 0);
                yield return request.SendWebRequest();

                if(!request.isHttpError && !request.isNetworkError)
                {
                    DownloadHandlerAssetBundle.GetContent(request);
                }
                else
                {
                    response(null);
                }
            }
            else
            {
                response(null);
            }
        }
        else
        {
            response(null);
        }
        request.Dispose();
    }
}
