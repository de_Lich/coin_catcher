﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BundleMyVar : MonoBehaviour
{
    private string url = "https://s3.eu-north-1.amazonaws.com/com.adelich.addressebles/Android/prefab.1";
    uint version = 0;
    void Start()
    {
        StartCoroutine(WebRequest(url, version));
    }

    IEnumerator WebRequest(string url, uint version)
    {
        while (!Caching.ready)
        {
            yield return null;
        }

        var request = UnityWebRequest.Get(url + ".manifest"); //here should be .json file


        if(!request.isHttpError && !request.isNetworkError)
        {
            Hash128 hash = default;

            //getting version
            var hashRow = request.downloadHandler.text.ToString().Split("\n".ToCharArray())[5];
            hash = Hash128.Parse(hashRow.Split(':')[1].Trim());

            if (hash.isValid == true)
            {
                request.Dispose();
                request = UnityWebRequestAssetBundle.GetAssetBundle(url, hash, 0);
                yield return request.SendWebRequest();

                if(!request.isHttpError && !request.isNetworkError)
                {
                    DownloadHandlerAssetBundle.GetContent(request);
                }
                else
                {
                    Debug.LogError("ne polilos zagruzit assety");
                }
            }
            else { Debug.Log("4to-to c hashem"); }
        }
        else 
        {
            string error = request.error;
            Debug.Log("4to-to s inetom" + error); 
        }
        request.Dispose();
    }

   
}
