﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // меню в первой сцене.
    private Transform cameraTransform;
    private Transform cameraDesiredLookAt;
    private AudioSource audioSource;
    private CanvasGroup canvasGroup;
    [SerializeField] Toggle toggleMusic;
    [SerializeField] Toggle toggleSound;


    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        cameraTransform = Camera.main.transform;
        canvasGroup = GetComponent<CanvasGroup>();
    }
    private void Update()
    {
        MoveCamera();
    }

    public void MoveCamera()
    {
        Vector3 cameraCorrectPos = new Vector3(0, 0, -77);
        if (cameraDesiredLookAt != null)
        {
            cameraTransform.transform.position = Vector3.Slerp(cameraTransform.position, cameraDesiredLookAt.position + cameraCorrectPos, 2f * Time.deltaTime);
        }

    }

    public void LookAtMenu(Transform menuTransform)
    {
        cameraDesiredLookAt = menuTransform;
    }
    public void SetMusic()
    {
        if (toggleMusic.isOn)
        {
            audioSource.Play();
        }
        else
        {
            audioSource.Stop();
        }
    }
    public void LoadLevel()
    {
        StartCoroutine(FadeMenu());
    }
    IEnumerator FadeMenu()
    {
        while (canvasGroup.alpha > 0)
        {
            canvasGroup.alpha -= Time.deltaTime;
            yield return null;
        }
        canvasGroup.interactable = false;
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("LoadingScene");
        canvasGroup.interactable = true;
    }
    public void SoundOff()
    {
        if (toggleSound.isOn)
        {
            AudioListener.pause = false;
        }
        else
        {
            AudioListener.pause = true;
        }
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
