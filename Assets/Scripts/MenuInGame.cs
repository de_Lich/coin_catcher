﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class MenuInGame : MonoBehaviour
{
    // меню во второй сцене. 
    [SerializeField] TextMeshProUGUI textCountAmount;
    private int countCoin = 0;
    void Start()
    {
        RandomMove.Score += Score;
    }
    public void Score()//отображение сбора монет
    {
        countCoin++;
        textCountAmount.text = countCoin.ToString();
    }
    public void ExitToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
