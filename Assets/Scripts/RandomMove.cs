﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class RandomMove : MonoBehaviour
{
    // движение персонажа
    NavMeshAgent nav;
    Animator animator;
    [SerializeField] float speed = 10f;
    [SerializeField] float xPoint, zPoint;
    public int coinCount;
    private AudioSource coinSound;
    private bool isReached = true;
    public delegate void ScoreDelegate();
    public static event ScoreDelegate Score;
    void Start()
    {
        animator = GetComponent<Animator>();
        nav = GetComponent<NavMeshAgent>();
        coinSound = GetComponent<AudioSource>();
        StartCoroutine(MoveToRandomPoint());
    }
    private void Update()
    {
        if (!isReached)
        {
            animator.SetBool("isMoving", true);
        }
        if (isReached)
        {
            animator.SetBool("isMoving", false);
        }
    }
    // генерация случайной точки назначение для navMeshAgent
    IEnumerator MoveToRandomPoint()
    {
        while (true)
        {
            if (isReached)
            {
                xPoint = Random.Range(1, 28);
                zPoint = Random.Range(1, 28);
                isReached = false;
            }
            Vector3 randomPoint = new Vector3(xPoint, 2, zPoint);
            nav.SetDestination(randomPoint);
            if (nav.transform.position.x == randomPoint.x && nav.transform.position.z == randomPoint.z)
            {
                isReached = true;
            }
            yield return new WaitForSeconds(1f);
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Coin")
        {
            coinSound.Play();
            coinCount++;
            Debug.Log("got a coin");
            Destroy(other);
            Score();
        }
    }
}
