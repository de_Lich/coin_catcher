﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingBar : MonoBehaviour
{
    // UI для промежуточного окна загрузки
    public Slider Slider;
    public float timer = 0f;
    public float interval = 3f;
    CanvasGroup CanvasGroup;

    public void Start()
    {
        CanvasGroup = GetComponent<CanvasGroup>();
    }

    private void Update()
    {
        timer += Time.deltaTime;
        SetLoading(timer);
        SetMaxLoad(timer);
        if (timer >= interval)
        {
            CanvasGroup.alpha -= Time.deltaTime;
            Invoke("LoadNextLevel", 1);
        }
    }
    private void LoadNextLevel()
    {
        SceneManager.LoadScene("FirstLevel");
    }

    public void SetMaxLoad(float load)
    {
        Slider.maxValue = interval;
        Slider.value = load;
    }
    public void SetLoading(float load)
    {
        Slider.value = load;
    }
}
