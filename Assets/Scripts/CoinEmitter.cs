﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinEmitter : MonoBehaviour
{
    //генерация монет на поле
    [SerializeField] GameObject coin;
    [SerializeField] float interval = 0.5f;
    [SerializeField] float xLength, zLength;
    float xDistance, zDistance;
    void Awake()
    {
        //вычисление области спауна монет 
        xLength = gameObject.transform.localScale.x;
        zLength = gameObject.transform.localScale.z;
    }
    private void Start()
    {
        StartCoroutine(CoinSpawn());
    }

    IEnumerator CoinSpawn()
    {
        Vector3 randomPos = Vector3.zero;
        while (true)
        {
            xDistance = Random.Range(0f, xLength);
            zDistance = Random.Range(0f, zLength);

            randomPos = new Vector3(xDistance, 8, zDistance);
            Instantiate(coin, randomPos, Quaternion.identity);
            yield return new WaitForSeconds(interval);
        }
    }
}
