﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BundleWebLoader : MonoBehaviour
{
    [SerializeField] Text warningTxt; 
    private string bundleURL = "https://s3.eu-north-1.amazonaws.com/com.adelich.addressebles/Android/prefab.4";
    private int version = 1;

    [System.Obsolete]
    void Start()
    {
        warningTxt = GetComponent<Text>();
        StartCoroutine(WebReq(version));
    }
    
    [System.Obsolete]
    IEnumerator WebReq(int version)
    {
        while (!Caching.ready)
        {
            Debug.Log("4to-to s chashem");
            yield return null;
        }
        var www = WWW.LoadFromCacheOrDownload(bundleURL, version);
        yield return www;
        if(www == null)
        {
            Debug.Log("net ineta");
            warningTxt.enabled = true;
        }
        while(www.isDone == false)
        {
            yield return null;
        }

        AssetBundle bundle = www.assetBundle;

        if(www.error == null)
        {
            GameObject[] obj = bundle.LoadAllAssets<GameObject>();
            foreach (var ob in obj)
            {
                Instantiate(ob);
            }
        }
        else
        {
            Debug.Log(www.error);
        }
        bundle.Unload(false);
    }
}
