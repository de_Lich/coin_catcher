﻿using UnityEngine;

public class Coin : MonoBehaviour
{
    //поведение монеты
    [SerializeField] float speedRotate = 180f;
    bool onGround = false;

    private void Update()
    {
        if (onGround)
        {
            transform.RotateAround(transform.position, new Vector3(0f, 1f, 0f), speedRotate * Time.deltaTime);
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            transform.RotateAround(transform.position, new Vector3(0f, 1f,0f), speedRotate * Time.deltaTime);
            onGround = true;
            Invoke("OnGround", 5f);
        }
    }
    private void OnGround()
    {
        Destroy(gameObject);
    }
}
